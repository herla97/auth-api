package mailers

import (
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/buffalo/render"
	"github.com/gobuffalo/packr/v2"
)

var smtp mail.Sender
var r *render.Engine

func init() {
	var err error

	// Pulling config from the env.
	port := os.Getenv("MAIL_PORT")
	host := os.Getenv("HOST")
	user := os.Getenv("MAIL_USERNAME")
	password := os.Getenv("MAIL_PASSWORD")

	smtp, err = mail.NewSMTPSender(host, port, user, password)
	if err != nil {
		log.Fatal(err)
	}

	r = render.New(render.Options{
		TemplatesBox: packr.New("../templates/mail", "../templates/mail"),
	})
}

// createCode random number generation function for verification code.
func createCode(low, hi int) int {
	rand.Seed(time.Now().UnixNano())
	return low + rand.Intn(hi-low)
}
