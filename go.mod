module auth_api

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gobuffalo/buffalo v0.15.5
	github.com/gobuffalo/buffalo-pop/v2 v2.0.6
	github.com/gobuffalo/envy v1.9.0
	github.com/gobuffalo/mw-contenttype v0.0.0-20190129203934-2554e742333b
	github.com/gobuffalo/mw-forcessl v0.0.0-20180802152810-73921ae7a130
	github.com/gobuffalo/mw-paramlogger v0.0.0-20190129202837-395da1998525
	github.com/gobuffalo/mw-tokenauth v0.0.0-20190129201951-95847f29c5c8
	github.com/gobuffalo/packr/v2 v2.8.0
	github.com/gobuffalo/pop/v5 v5.1.1
	github.com/gobuffalo/suite/v3 v3.0.0
	github.com/gobuffalo/validate/v3 v3.3.0
	github.com/gobuffalo/x v0.0.0-20190224155809-6bb134105960
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/markbates/grift v1.5.0
	github.com/pkg/errors v0.9.1
	github.com/rs/cors v1.7.0
	github.com/unrolled/secure v0.0.0-20190103195806-76e6d4e9b90c
	golang.org/x/crypto v0.0.0-20200320181102-891825fb96df
)
