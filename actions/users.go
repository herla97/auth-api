package actions

import (
	"auth_api/mailers"
	"auth_api/models"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop/v5"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// UsersCreate default implementation.
func UsersCreate(c buffalo.Context) error {
	// User Model
	user := &models.User{}

	// Bind user to the json elements.
	if err := c.Bind(user); err != nil {
		return errors.WithStack(err)
	}
	// Get the DB connection from the context.
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}
	// Validate and create the data.
	verrs, err := tx.ValidateAndCreate(user)
	if err != nil {
		return errors.WithStack(err)
	}

	// verrs.HasAny returns true/false depending on whether any errors
	// have been tracked.
	if verrs.HasAny() {
		c.Set("errors", verrs)
		return c.Error(http.StatusConflict, errors.New(verrs.Error()))
	}

	return c.Render(http.StatusCreated, r.Auto(c, map[string]string{"message": "User Created"}))
}

// UsersRead default implementation.
func UsersRead(c buffalo.Context) error {
	users := &models.Users{}

	// Get the DB connection from the context.
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}

	// Paginate results. Params "page" and "per_page" control pagination.
	// Default values are "page=1" and "per_page=20".
	// Add Order for date.
	q := tx.PaginateFromParams(c.Params()).Order("created_at asc")

	// Retrieve all Users from the DB. Select all except password.
	if err := q.Select(
		"id",
		"created_at",
		"updated_at",
		"username",
		"email",
	).All(users); err != nil {
		return errors.WithStack(err)
	}
	// Add the paginator to the context so it can be used in the template.
	c.Set("pagination", q.Paginator)

	return c.Render(http.StatusOK, r.Auto(c, users))
}

// UsersReadByID default implementation.
func UsersReadByID(c buffalo.Context) error {
	user := &models.User{}

	// Get the DB connection from the context.
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}
	// Retrieve a User from the DB. Select all except password. Using parameter "user_id".
	if err := tx.Select(
		"id",
		"created_at",
		"updated_at",
		"username",
		"email",
	).Find(user, c.Param("user_id")); err != nil {
		return c.Error(http.StatusNotFound, err)
	}

	return c.Render(http.StatusOK, r.Auto(c, user))
}

// UsersUpdate default implementation.
func UsersUpdate(c buffalo.Context) error {
	// Get the DB connection from the context.
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}
	// Allocate an empty User
	user := &models.User{}
	if err := tx.Find(user, c.Param("user_id")); err != nil {
		return c.Error(http.StatusNotFound, err)
	}

	// Bind Framework to the html form elements
	if err := c.Bind(user); err != nil {
		return errors.WithStack(err)
	}

	// Validate the data and exclude colum password.
	verrs, err := tx.ValidateAndUpdate(user, "password")
	if err != nil {
		return errors.WithStack(err)
	}

	// verrs.HasAny returns true/false depending on whether any errors
	// have been tracked.
	if verrs.HasAny() {
		c.Set("errors", verrs)
		return c.Error(http.StatusConflict, errors.New(verrs.Error()))
	}

	return c.Render(http.StatusCreated, r.Auto(c, map[string]string{"message": "User Updated"}))
}

// UsersDelete default implementation.
func UsersDelete(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}
	// Allocate an empty User
	user := &models.User{}

	// To find the Widget the parameter widget_id is used.
	if err := tx.Find(user, c.Param("user_id")); err != nil {
		return c.Error(404, err)
	}

	if err := tx.Destroy(user); err != nil {
		return errors.WithStack(err)
	}

	return c.Render(http.StatusCreated, r.Auto(c, map[string]string{"message": "User Deleted"}))
}

// UsersForgotPassword default implementation.
func UsersForgotPassword(c buffalo.Context) error {
	// Allocate an empty User
	user := &models.User{}

	// Bind Framework to the html form elements
	if err := c.Bind(user); err != nil {
		return errors.WithStack(err)
	}

	// Save var of Request JSON Post.
	useremail := user.Email

	if useremail == "" {
		return c.Error(http.StatusBadRequest, errors.New("Username cannot be empty"))
	}

	// Get the DB connection from the context.
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}

	q := tx.Select("id").Where("email = ?", useremail)

	if err := q.First(user); err != nil {
		return c.Error(http.StatusNotFound, errors.New("User Not Found"))
	}

	// Send verification code
	codeHashString, err := mailers.SendCode(useremail)
	if err != nil {
		return errors.WithStack(err)
	}

	// Add values to validation model.
	validation := &models.Validation{
		User:          user.ID,
		Code:          codeHashString,
		ExpirationdAt: time.Now().Add(5 * time.Minute),
	}

	// Insert user id, code hash into validations
	err = tx.Create(validation)
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(http.StatusOK, r.Auto(c, map[string]string{
		"message": "We send a verification code to your email",
		"user_id": user.ID.String()}))
}

// UsersUpdatePassword default implementation.
func UsersUpdatePassword(c buffalo.Context) error {
	// Get the DB connection from the context.
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}

	// Allocate an empty User
	user := &models.User{}

	claims := c.Value("claims").(jwt.MapClaims)

	t := claims["type"].(string)
	if t != "password" {
		return c.Error(http.StatusUnauthorized, errors.New("Invalid Token"))
	}

	// Bind Framework to the html form elements
	if err := c.Bind(user); err != nil {
		return errors.WithStack(err)
	}

	// Validation password size
	if len(user.Password) < 6 || len(user.Password) > 50 {
		return c.Error(http.StatusNotAcceptable, errors.New("The password must be greater than 6 characters"))
	}

	// find auth user with the user_id and user not soft deleted.
	if err := tx.Select("id").Find(user, c.Param("user_id")); err != nil {
		return c.Error(http.StatusNotFound, err)
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return errors.WithStack(err)
	}

	ph := string(hash)

	// RawQuery for update password user.
	err = tx.RawQuery("UPDATE users SET password = ? WHERE id = ?", ph, user.ID).Exec()
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(http.StatusCreated, r.Auto(c, map[string]string{"message": "Updated Password"}))
}
