package actions

import (
	"auth_api/models"
	"net/http"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop/v5"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// ValidationsForgotPasswordCode default implementation.
func ValidationsForgotPasswordCode(c buffalo.Context) error {
	// Get the DB connection from the context.
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no se encontro ninguna conexión"))
	}
	// Get the JWT Key Secret from .env file.
	secret := os.Getenv("JWT_SECRET")

	// User Model
	user := &models.User{}

	// Validation Model
	validation := &models.Validation{}

	// Bind Framework to the html form elements
	if err := c.Bind(validation); err != nil {
		return errors.WithStack(err)
	}

	// Save input raw code from user.
	code := validation.Code

	q := tx.Order("created_at desc").Where("expiration_at >= ? and id_user = ?", time.Now(), c.Param("user_id"))

	if err := q.First(validation); err != nil {
		return c.Error(http.StatusNotFound, errors.New("Code expired"))
	}

	// find auth user with the user_id and user not soft deleted.
	if err := tx.Select("id").Find(user, c.Param("user_id")); err != nil {
		return c.Error(http.StatusNotFound, err)
	}

	// confirm that the given password matches the hashed password from the db
	if err := bcrypt.CompareHashAndPassword([]byte(validation.Code), []byte(code)); err != nil {
		return c.Error(http.StatusBadRequest, errors.New("Código de Validadación Invalido"))
	}

	// Generate token with 6 hours expiration time.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":   user.ID,
		"exp":  time.Now().Add(time.Minute * 5).Unix(),
		"type": "password",
	})

	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		return errors.WithStack(err)
	}

	return c.Render(http.StatusOK, r.Auto(c, map[string]string{
		"message": "Verified User",
		"user_id": user.ID.String(),
		"token":   tokenString}))
}
